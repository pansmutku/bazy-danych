# Bazy Danych

Strukruta database.sql <--- Graficzna manifestacja relacyjności przykładowej bazy MySQL.

<img src="https://gitlab.com/pansmutku/bazy-danych/raw/master/db.png" title="Bazy Danych" alt="Bazy Danych" width="100%" height="100%">

## Model Relacyjny 

<strong>Model relacyjny</strong> – model organizacji danych bazujący na matematycznej teorii mnogości, w szczególności na pojęciu relacji. Na modelu relacyjnym oparta jest relacyjna baza danych (ang. Relational Database) – baza danych, w której dane są przedstawione w postaci relacyjnej.

W najprostszym ujęciu w modelu relacyjnym dane grupowane są w relacje, które reprezentowane są przez tablice. Relacje są pewnym zbiorem rekordów o identycznej strukturze wewnętrznie powiązanych za pomocą związków zachodzących pomiędzy danymi. Relacje zgrupowane są w tzw. schematy bazy danych. Relacją może być tabela zawierająca dane teleadresowe pracowników, zaś schemat może zawierać wszystkie dane dotyczące firmy. Takie podejście w porównaniu do innych modeli danych ułatwia wprowadzanie zmian, zmniejsza możliwość pomyłek, ale dzieje się to kosztem wydajności. 

Relacyjny model danych <b><i>jest obecnie najbardziej popularnym modelem używanym w systemach baz danych</b></i>. Podstawą tego modelu stała się praca opublikowana przez E.F. Coddaw 1970r. Zauważył on, że zastosowanie struktur i procesów matematycznych w zarządzaniu danymi mogłoby rozwiązaćwiele problemów trapiących ówczesne modele. W swej pracy <b><i>A relational model for large shared data banks</i></b> Codd zaprezentował założenia relacyjnego modelu baz danych, model ten oparł na teorii mnogości i rachunku predykatów pierwszego rzędu.