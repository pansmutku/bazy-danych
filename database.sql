-- Marcin Woźniak.


CREATE TABLE `Customer` (
-- Tworzymy tabele Customer
    `CustomerID` int  NOT NULL ,
    -- Tworzymy Customer ID
    `Name` string  NOT NULL ,
    -- Tworzymy stringa Name
    `Address1` string  NOT NULL ,
     -- Tworzymy stringa Address1
    `Address2` string  NULL ,
     -- Tworzymy stringa Address2
    `Address3` string  NULL ,
     -- Tworzymy stringa Address3
    PRIMARY KEY (
     -- Ustawiamy klucz główny na CustomerID
        `CustomerID`
    )
);

CREATE TABLE `Order` (
 -- Tworzymy tabele Order
    `OrderID` int  NOT NULL ,
     -- Tworzymy inta [liczba całkowita] OrderID
    `CustomerID` int  NOT NULL ,
     -- Tworzymy inta [liczba całkowita] CustomerID
    `TotalAmount` money  NOT NULL ,
    -- Tworzymy moneya [typ przechowujący pieniądze $$$] CustomerID
    `OrderStatusID` int  NOT NULL ,
    -- Tworzymy inta OrderStatusID
    
    PRIMARY KEY (
        -- Ustawiamy klucz główny na OrderID
        `OrderID`
        
    )
);

CREATE TABLE `OrderLine` (
 -- Tworzymy tabele OrderLine
    `OrderLineID` int  NOT NULL ,
    -- Tworzymy inta [liczba całkowita] OrderLineID
    `OrderID` int  NOT NULL ,
    -- Tworzymy inta [liczba całkowita] OrderID
    `ProductID` int  NOT NULL ,
    -- Tworzymy inta [liczba całkowita] ProductID
    `Quantity` int  NOT NULL ,
    -- Tworzymy inta [liczba całkowita] Quantity [ilosc]
    PRIMARY KEY (
    -- Ustawiamy klucz główny na OrderLineID
        `OrderLineID`
    )
);


CREATE TABLE `Product` (
-- Tworzymy tabele Product
    `ProductID` int  NOT NULL ,
    -- Tworzymy inta [liczba całkowita] ProductID
    `Name` varchar(200)  NOT NULL ,
    -- Tworzymy varchar [zajmuje tyle miejsca ile danych wypełnimy] Name
    `Price` money  NOT NULL ,
    -- Tworzymy money Price
    PRIMARY KEY (
    -- Ustawiamy klucz główny ProductID
        `ProductID`
    ),
    CONSTRAINT `uc_Product_Name` UNIQUE (
    -- Ustawiamy, aby Name nie powtarzało się - było unikatowe.
        `Name`
    )
);

CREATE TABLE `OrderStatus` (
-- Tworzymy tabele OrderStatus
    `OrderStatusID` int  NOT NULL ,
    -- Tworzymy inta [liczba całkowita] OrderStatusID
    `Name` string  NOT NULL ,
     -- Tworzymy stringa [ ciąg znaków ] Name
    PRIMARY KEY (
     -- Ustawiamy klucz główny na OrderStatusID
        `OrderStatusID`
    ),
    CONSTRAINT `uc_OrderStatus_Name` UNIQUE (
     -- Ustawiamy, aby Name nie powtarzło się - było unikatowe. 
        `Name`
    )
);

 -- Wykonujemy relacje między poszczególnymi tabelami
 -- Zasada jest taka. Wybieramy tabele i konkretne pole, do którego dopisujemy połączenie z kluczem obcym i definiujemy gdzie ten klucz obcy się znajduje [w jakiej tabeli]. 
ALTER TABLE `Order` ADD CONSTRAINT `fk_Order_CustomerID` FOREIGN KEY(`CustomerID`)
REFERENCES `Customer` (`CustomerID`);

ALTER TABLE `Order` ADD CONSTRAINT `fk_Order_OrderStatusID` FOREIGN KEY(`OrderStatusID`)
REFERENCES `OrderStatus` (`OrderStatusID`);

ALTER TABLE `OrderLine` ADD CONSTRAINT `fk_OrderLine_OrderID` FOREIGN KEY(`OrderID`)
REFERENCES `Order` (`OrderID`);

ALTER TABLE `OrderLine` ADD CONSTRAINT `fk_OrderLine_ProductID` FOREIGN KEY(`ProductID`)
REFERENCES `Product` (`ProductID`);

 -- Dodajemy Index
CREATE INDEX `idx_Customer_Name`
ON `Customer` (`Name`);

